<?php
include('inc/header.php')
?>

<main class="main-content site-wrapper-reveal">
      <!--== Start Hero Area Wrapper ==-->
      <section class="page-title-area position-relative bgcolor-gray-silver bg-img p-0" data-bg-img="assets/img/slider/bg-1.jpg"
        style="background-image: url('assets/img/photos/page-title-02.jpg');max-width:100%;">
        <div class="bg-op-color"></div>
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title-content page-content-style4 text-center">
                <h2 class="title">
                  The fastest way to top the portal, Backlink Pro</h2>
                <h6 class="">
                  ith just a few clicks, you can use the backlink service for portal top exposure!
                </h6>
                <div class="buttons">
                  <a href="portfolio-page.php" class="btn btn-success m-2">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Result of work success 🏆</font>
                    </font>
                  </a>
                  <a href="login.php" class="btn btn-info bg-info m-2">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Get Started for Free</font>
                    </font>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== End Hero Area Wrapper ==-->
      <section class="about-area about-isometric-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="thumb wow fadeInLeft  animated" style="visibility: visible; animation-name: fadeInLeft;">
                <img class="icon-img img-right-align" src="assets/img/about/v02.png" alt="Boseo-HasTech">
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="section-title mt-35 ml-30 md-ml-0">
                <h2 class="title">What is a backlink operation?</h2>
                <div class="accordian-content">
                  <div class="accordion" id="accordionStyle">

                    <div class="accordion-item">
                      <div class="accordian-heading" id="headingOne">
                        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseOne"
                          aria-expanded="false" aria-controls="collapseOne">
                          What is a backlink?
                          <i class="icon icon-plus icon_plus"></i>
                          <i class="icon icon-minus icon_minus-06"></i>
                        </button>
                      </div>
                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionStyle">
                        <div class="accordian-content">
                          Means citations or references to my site by other sites.
                          As such, backlinks act as word of mouth in website search results.
                        </div>
                      </div>
                    </div>

                    <div class="accordion-item">
                      <div class="accordian-heading" id="headingTwo">
                        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo"
                          aria-expanded="false" aria-controls="collapseTwo">
                          Increase website awareness
                          <i class="icon icon-plus icon_plus"></i>
                          <i class="icon icon-minus icon_minus-06"></i>
                        </button>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionStyle">
                        <div class="accordian-content">
                          If you build awareness through continuous backlink work,
                          you can expose up to the final page in the search results of the portal site.
                        </div>
                      </div>
                    </div>

                    <div class="accordion-item">
                      <div class="accordian-heading" id="headingThree">
                        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree"
                          aria-expanded="false" aria-controls="collapseThree">
                          1 page impression -> visitor increase!
                          <i class="icon icon-plus icon_plus"></i>
                          <i class="icon icon-minus icon_minus-06"></i>
                        </button>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordionStyle">
                        <div class="accordian-content">
                          One page impression increases the number of visitors,
                          which in turn leads to an increase in sales.
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== Start Service Area ==-->
      <!-- <section class="service-area service-seo-area">
      <div class="container">
        <div class="row icon-box-style4">
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/11.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">Organic Search (SEO)</a></h4>
                <p>Stop selling. Start helping. Utilize SEO to capture early stage awareness queries through relevant content</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/12.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">Link Building Services</a></h4>
                <p>Our full-service link building company specializes in custom link building campaigns that earn high-quality links</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/13.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">Paid Search (PPC)</a></h4>
                <p>Search engine marketing (SEM, which can include SEO), pay-per-click (PPC), search engine advertising sponsored</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/38.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">SEO Auditing Services</a></h4>
                <p>If you haven't recently audited you website, we recommend an SEO audit before you begin a campaign</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/39.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">B2B SEO Services</a></h4>
                <p>B2B SEO campaign will include a study of business goals, followed by an evaluation of pre-purchase</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="icon-box-item">
              <div class="icon-box">
                <img class="icon-img" src="assets/img/icons/40.png" alt="Icon">
              </div>
              <div class="content">
                <h4><a href="#/">Full Service SEO</a></h4>
                <p>Based in Americas, Boseo offers full service SEO marketing solutions for companies of all sizes and types</p>
                <a href="#/" class="btn btn-link">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="shape-layer">
        <div class="layer layer-one">
          <img src="assets/img/shape/bg-shape-06.png" alt="Boseo-HasTech">
        </div>
      </div>
    </section> -->
      <!--== End Service Area ==-->

      <!--== Start About Area ==-->
      <!-- <section class="about-area about-seo-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="thumb wow fadeInLeft animated">
                <img class="icon-img" src="assets/img/about/v03.png" alt="Boseo-HasTech">
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="section-title">
                <h5>Why Backlink Pro ?</h5>
                <h2 class="title">After creating a website, how do I get customers to my site?</h2>
                <div class="desc pr-30">
                  <p>Blog ads, paid click ads, SNS ads, Instagram ads, etc. are really diverse.
                    But what if my website is not displayed on the first page of the portal site? All marketing will go
                    to waste.
                    In order to solve this inconvenience, Backlink Pro has
                    succeeded in researching and developing a backlink service that allows your website to reach the
                    first page when searching for a business name or specific keywords .
                    You can apply for a backlink job within 1 minute of signup without the need for complicated setup.
                    Because of the continuous quality control, you can expect a meaningful increase in ranking in the
                    search results of websites such as Google and domestic portals.</p>
                  <div>
                    <a href="about.html" class="btn btn-theme mr-30 mr-xs-15 mb-xs-30">More About Us</a>
                    <a href="https://player.vimeo.com/video/174392490?dnt=1&amp;app_id=122963"
                      class="btn btn-link no-line play-video-popup mb-xs-30"><img src="assets/img/icons/play.png"
                        alt="Icon">See Video</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> -->
      <!--== End About Area ==-->

      <!--== Start Funfact Area ==-->
      <section class="funfact-area funfact-seo-area">
        <div class="container">
          <div class="row">
            <div class="col-6 col-lg-3">
              <div class="funfact-item">
                <div class="number">
                  <h2><span class="counter-animate">3254</span>+</h2>
                </div>
                <p class="title">Completed Projects</p>
              </div>
            </div>
            <div class="col-6 col-lg-3">
              <div class="funfact-item">
                <div class="number">
                  <h2><span class="counter-animate">59</span></h2>
                </div>
                <p class="title">Award Winner</p>
              </div>
            </div>
            <div class="col-6 col-lg-3">
              <div class="funfact-item">
                <div class="number">
                  <h2><span class="counter-animate">234</span>k</h2>
                </div>
                <p class="title">Worldwide Users</p>
              </div>
            </div>
            <div class="col-6 col-lg-3">
              <div class="funfact-item">
                <div class="number">
                  <h2><span class="counter-animate">825</span></h2>
                </div>
                <p class="title">Satisfied Clients</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== End Funfact Area ==-->

      <!--== Start Portfolio Area ==-->
      <!-- <section class="portfolio-area portfolio-seo-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="section-title">
                <h5>Case Studies</h5>
                <h2 class="title">Our Latest Projects</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid p-0">
          <div class="row">
            <div class="col-lg-12">
              <div class="portfolio-slider portfolio-slider-seo owl-carousel owl-theme owl-nav3">
                <div class="item">
                  <div class="portfolio-item">
                    <div class="inner-content">
                      <div class="thumb">
                        <a href="portfolio-details.html"><img src="assets/img/portfolio/vector01.jpg"
                            alt="Boseo-Portfolio" /></a>
                      </div>
                      <div class="portfolio-info hover-theme-color2">
                        <h3 class="title"><a href="portfolio-details.html">Barclays Capital Group</a></h3>
                        <a href="portfolio-details.html" class="category">Local SEO</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="portfolio-item">
                    <div class="inner-content">
                      <div class="thumb">
                        <a href="portfolio-details.html"><img src="assets/img/portfolio/vector02.jpg"
                            alt="Boseo-Portfolio" /></a>
                      </div>
                      <div class="portfolio-info hover-theme-color2">
                        <h3 class="title"><a href="portfolio-details.html">Barclays Capital Group</a></h3>
                        <a href="portfolio-details.html" class="category">Local SEO</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="portfolio-item">
                    <div class="inner-content">
                      <div class="thumb">
                        <a href="portfolio-details.html"><img src="assets/img/portfolio/vector03.jpg"
                            alt="Boseo-Portfolio" /></a>
                      </div>
                      <div class="portfolio-info hover-theme-color2">
                        <h3 class="title"><a href="portfolio-details.html">Barclays Capital Group</a></h3>
                        <a href="portfolio-details.html" class="category">Local SEO</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="portfolio-item">
                    <div class="inner-content">
                      <div class="thumb">
                        <a href="portfolio-details.html"><img src="assets/img/portfolio/vector04.jpg"
                            alt="Boseo-Portfolio" /></a>
                      </div>
                      <div class="portfolio-info hover-theme-color2">
                        <h3 class="title"><a href="portfolio-details.html">Barclays Capital Group</a></h3>
                        <a href="portfolio-details.html" class="category">Local SEO</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="portfolio-item">
                    <div class="inner-content">
                      <div class="thumb">
                        <a href="portfolio-details.html"><img src="assets/img/portfolio/vector05.jpg"
                            alt="Boseo-Portfolio" /></a>
                      </div>
                      <div class="portfolio-info hover-theme-color2">
                        <h3 class="title"><a href="portfolio-details.html">Barclays Capital Group</a></h3>
                        <a href="portfolio-details.html" class="category">Local SEO</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="shape-layer">
          <div class="layer layer-one">
            <img src="assets/img/shape/bg-shape-07.png" alt="Boseo-HasTech">
          </div>
        </div>
      </section> -->
      <!--== End Portfolio Area ==-->
  <!--== Start Pricing Area Wrapper ==-->
  <section class="pricing-area">
    <div class="container-fluid pt-125 pt-md-100">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-80 mb-md-50">
            <h5>Pricing Table</h5>
            <h2 class="title">Choose Your Best Plan</h2>
          </div>
        </div>
      </div>
      <div class="row justify-content-md-center">

        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/orderPerfect.php">
                    <img src="assets/img/orderPerfect.png" alt="Perfect Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Perfect Backlink Order </font>
                    </font><span class="badge badge-success">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">NEW</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We are working on artificial intelligence-based perfect backlinks to domestic sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 1-3 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">high ranking effect</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Rank Up Only</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Artificial intelligence (AI) based document creation</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">100% Do-Follow Backlink</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦160,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderPerfect.php" class="btn btn-primary">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">

                  <a href="/orderKorea.php">
                    <img src="assets/img/orderKorea.png" alt="Domestic Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Domestic Backlink Order </font>
                    </font><span class="badge badge-warning">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">UPDATE</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We work on backlinks to domestic community sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★ (Normal)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 2-4 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Intensive work on domestic portals</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Domestic portal ranking rise &amp; keyword exposure for the first time</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">domain score random</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">100% Do-Follow Backlink</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦99,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderKorea.php" class="btn btn-success">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/order.php">
                    <img src="assets/img/orderGlobal.png" alt="Global Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Global Backlink Order
                      </font>
                    </font>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We provide various types of backlinks to overseas sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★ (Normal)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 4-8 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">reasonable price</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Google Ranking Up &amp; Keywords First Exposure</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">High domain score </font>
                      </font><small>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">(varies by product)</font>
                        </font>
                      </small>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">various backlinks</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦50,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./order.php" class="btn btn-default">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/orderFree.php">
                    <img src="assets/img/orderFree.png" alt="FREE backlink thumbnails" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        FREE backlink order </font>
                    </font><span class="badge badge-success">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">NEW</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We are working on artificial intelligence-based perfect backlink samples on domestic sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Perfect Backlink Sample</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">2 free works when registering as a new member</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦0
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderPerfect.php?type=free" class="btn btn-secondary">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Pricing Area Wrapper ==-->
      <!--==how tro page -1==-->
      <section class="h-t-u py-5">
        <div class="container">
          <h1 class=" text-center py-3"> Those who need backlink work</h1>
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="post-item">
                <div class="thumb">
                  <a href="blog-details.html"><img src="assets/img/blog/m03.jpg" alt="Boseo-Blog"></a>
                </div>
                <div class="content">
  
                  <h4 class="title">
                    <a href="blog-details.html">Optimizing Pinterest for the Buyer’s Journey</a>
                  </h4>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="post-item">
                <div class="thumb">
                  <a href="blog-details.html"><img src="assets/img/blog/m03.jpg" alt="Boseo-Blog"></a>
                </div>
                <div class="content">
  
                  <h4 class="title">
                    <a href="blog-details.html">Optimizing Pinterest for the Buyer’s Journey</a>
                  </h4>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="post-item">
                <div class="thumb">
                  <a href="blog-details.html"><img src="assets/img/blog/m03.jpg" alt="Boseo-Blog"></a>
                </div>
                <div class="content">
  
                  <h4 class="title">
                    <a href="blog-details.html">Optimizing Pinterest for the Buyer’s Journey</a>
                  </h4>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </section>
      <!--==how tro page -1==-->

      <!--== Start Testimonial Area ==-->
      <section class="testimonial-area testimonial-seo-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="section-title mb-30">
                <h5>Testimonials</h5>
                <h2 class="title">Trusted From My Clients</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid p-0">
          <div class="row">
            <div class="col-lg-12">
              <div class="testimonial-content">
                <div class="testimonial-slider-two owl-carousel owl-theme owl-nav3">
                  <div class="testimonial-carousel-item">
                    <!--== Start Testimonial Item ==-->
                    <div class="testimonial-item">
                      <div class="client-content">
                        <p>“Kelly Chandler is definitely an expert in digital marketing! She is always doing a great job
                          and takes time to explain her thoughts and her process. She doesn't hesitate to share new
                          ideas and be proactive all the time, which is great when you want to grow your business. Thank
                          you very much.”</p>
                      </div>
                      <div class="client-info">
                        <div class="thumb">
                          <img src="assets/img/testimonial/01.jpg" alt="Image">
                        </div>
                        <div class="desc">
                          <h4 class="name">Chris Carroll</h4>
                          <p class="designation">CEO Founder at Google Inc</p>
                        </div>
                        <div class="icon-quote"><img src="assets/img/icons/right-quote03.png" alt="Image"></div>
                      </div>
                    </div>
                    <!--== End Testimonial Item ==-->
                  </div>

                  <div class="testimonial-carousel-item">
                    <!--== Start Testimonial Item ==-->
                    <div class="testimonial-item">
                      <div class="client-content">
                        <p>“We’ve been with higher visibility for about two months now and with the help of our account
                          manager Lauren Moscato, we’ve been able to improve our website and rankings dramatically. We
                          will be continuing business with them and are excited to see what’s to come. Thank you very
                          much.”</p>
                      </div>
                      <div class="client-info">
                        <div class="thumb">
                          <img src="assets/img/testimonial/02.jpg" alt="Image">
                        </div>
                        <div class="desc">
                          <h4 class="name">Julia Steve</h4>
                          <p class="designation">Manager at Spotify LLC</p>
                        </div>
                        <div class="icon-quote"><img src="assets/img/icons/right-quote03.png" alt="Image"></div>
                      </div>
                    </div>
                    <!--== End Testimonial Item ==-->
                  </div>

                  <div class="testimonial-carousel-item">
                    <!--== Start Testimonial Item ==-->
                    <div class="testimonial-item">
                      <div class="client-content">
                        <p>“A company that delivers results from the moment you start. We had been managing our own
                          accounts and our SEO had only been a small part of our efforts. With Higher Visibility a
                          weight was lifted off our shoulders on the campaigns they now manage and our SEO ranking is
                          growing daily.”</p>
                      </div>
                      <div class="client-info">
                        <div class="thumb">
                          <img src="assets/img/testimonial/03.jpg" alt="Image">
                        </div>
                        <div class="desc">
                          <h4 class="name">Anna Houston</h4>
                          <p class="designation">Senior Executive at Dropbox</p>
                        </div>
                        <div class="icon-quote"><img src="assets/img/icons/right-quote03.png" alt="Image"></div>
                      </div>
                    </div>
                    <!--== End Testimonial Item ==-->
                  </div>

                  <div class="testimonial-carousel-item">
                    <!--== Start Testimonial Item ==-->
                    <div class="testimonial-item">
                      <div class="client-content">
                        <p>“I wanted to take a moment and let you know how happy we are. You have show great knowledge
                          in regards to SEO best practices as well as optimization strategy and management of the
                          BOSEO.com account. Most importantly, you have obtained results, and that is an undeniable
                          measure.”</p>
                      </div>
                      <div class="client-info">
                        <div class="thumb">
                          <img src="assets/img/testimonial/04.jpg" alt="Image">
                        </div>
                        <div class="desc">
                          <h4 class="name">Ed Dodd</h4>
                          <p class="designation">CEO at Ridegear.com</p>
                        </div>
                        <div class="icon-quote"><img src="assets/img/icons/right-quote03.png" alt="Image"></div>
                      </div>
                    </div>
                    <!--== End Testimonial Item ==-->
                  </div>
                </div>
                <div class="layer-shape">
                  <img class="layer-shape-one" src="assets/img/shape/bg-shape-02.png" alt="Boseo-Image">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== End Testimonial Area ==-->

      <!--== Start Brand Logo Area ==-->
      <section class="brand-logo-area brand-seo-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="brand-logo-content-area">
                <div class="brand-list brand-logo-slider owl-carousel owl-theme">
                  <div class="brand-logo-item">
                    <a href="#/"><img src="assets/img/brand-logo/1.png" alt="Boseo-Logo" /></a>
                  </div>

                  <div class="brand-logo-item">
                    <a href="#/"><img src="assets/img/brand-logo/2.png" alt="Boseo-Logo" /></a>
                  </div>

                  <div class="brand-logo-item">
                    <a href="#/"><img src="assets/img/brand-logo/3.png" alt="Boseo-Logo" /></a>
                  </div>

                  <div class="brand-logo-item">
                    <a href="#/"><img src="assets/img/brand-logo/4.png" alt="Boseo-Logo" /></a>
                  </div>

                  <div class="brand-logo-item">
                    <a href="#/"><img src="assets/img/brand-logo/5.png" alt="Boseo-Logo" /></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== Start Brand Logo Area ==-->



      <!--== Start Divider Area ==-->
      <!-- <section class="divider-area divider-seo-report-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 offset-lg-2">
              <div class="seo-report-form-content">
                <form class="seo-report-form" action="https://htmldemo.hasthemes.com/boseo/boseo/php/mail.php"
                  method="post">
                  <h4>Free SEO Reports</h4>
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_website" placeholder="Website *">
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_keyword" placeholder="Keyword">
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_name" placeholder="Name *">
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_email" placeholder="Email *">
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_phone" placeholder="Phone">
                  </div>
                  <div class="form-group">
                    <button class="btn-theme" type="submit">Scan Now</button>
                  </div>
                </form>
                <div class="shape-layer">
                  <div class="layer layer-one">
                    <img src="assets/img/shape/d01.png" alt="Boseo-HasTech">
                  </div>
                  <div class="layer layer-two">
                    <img src="assets/img/shape/d02.png" alt="Boseo-HasTech">
                  </div>
                  <div class="layer layer-three">
                    <img src="assets/img/shape/d03.png" alt="Boseo-HasTech">
                  </div>
                  <div class="layer layer-four">
                    <img src="assets/img/shape/d04.png" alt="Boseo-HasTech">
                  </div>
                  <div class="layer layer-five">
                    <img src="assets/img/shape/d05.png" alt="Boseo-HasTech">
                  </div>
                  <div class="layer layer-six">
                    <img src="assets/img/shape/d06.png" alt="Boseo-HasTech">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
              <div class="section-title">
                <h5>SEO Reports</h5>
                <h2 class="title">How much more <br>traffic should you actually <br>be getting</h2>
                <div class="desc pr-30">
                  <p>These free SEO reports are also called as free SEO audits at times. They comprise some many
                    important features as mentioned below</p>
                </div>
                <div class="list-style">
                  <ul>
                    <li><i class="arrow_carrot-down_alt2"></i> Site architecture</li>
                    <li><i class="arrow_carrot-down_alt2"></i> Web content issues</li>
                    <li><i class="arrow_carrot-down_alt2"></i> Usability issues</li>
                    <li><i class="arrow_carrot-down_alt2"></i> Site navigation issues</li>
                  </ul>
                  <ul>
                    <li><i class="arrow_carrot-down_alt2"></i> Inbound link analysis</li>
                    <li><i class="arrow_carrot-down_alt2"></i> Outbound link analysis</li>
                    <li><i class="arrow_carrot-down_alt2"></i> Site domain issues</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> -->
      <!--== End Divider Area ==-->



      <!--== Start Divider Area Wrapper ==-->
      <section class="divider-area divider-quote-area">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="quote-content text-center">
                <div class="row">
                  <div class="col-lg-6 m-auto">
                    <h2>Come Work With Us!</h2>
                    <form class="input-btn-group">
                      <input class="form-control no-border" type="text" placeholder="Your Email">
                      <button class="btn btn-theme" type="button">Request a quote</button>
                    </form>
                  </div>
                </div>
                <img class="bg-shape-img" src="assets/img/shape/bg-shape-01.png" alt="Boseo-Image">
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--== End Divider Area Wrapper ==-->
    </main>

<?php
include('inc/footer.php')
?>