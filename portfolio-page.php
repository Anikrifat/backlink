<?php
include('inc/header.php')
?>

<main class="main-content site-wrapper-reveal">
  <!--== Start Page Title Area ==-->
  <section class="page-title-area">
    <div class="container">
      <div class="row ">
        <div class="col-lg-12 text-center">
          <div class="page-title-content">
            <h2 class="title">📈 Backlink work result portfolio🏆</h2>
            <h6 class="font-weight-400">The best is different. We will show you the best results.🤝</h6>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Page Title Area ==-->

  <section class="protfolio py-3">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="#portfolio1" data-toggle="modal" data-target="#portfolio1">
                    <img src="assets/img/portfolio-1.png" alt="Portfolio - Sports" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400"><img src="assets/img/google-logo.png" class="img-reponsive img-logo" style="max-width: 1.5rem;">

                    : 1st place on page 1!
                    [Sports related]🥇


                  </h6>
                  <div class="h6 font-weight-300">
                    <p class="ni location_pin mr-2">


                      It is a person who has benefited from the sports information community that has the highest competition rate in Korea.
                      <br>


                      🐱&zwj;🏍 Existing 3 pages -&gt; After work, I saw a dramatic synergistic effect as the key keyword ranked first.

                    </p>
                  </div>
                  <hr class="my-4">
                  <a href="#portfolio1" class="btn btn-theme" data-toggle="modal" data-target="#portfolio1">
                    View more
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-12 col-md-4 col-lg-4">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="#portfolio1" data-toggle="modal" data-target="#portfolio1">
                    <img src="assets/img/portfolio-1.png" alt="Portfolio - Sports" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400"><img src="assets/img/google-logo.png" class="img-reponsive img-logo" style="max-width: 1.5rem;">
                    : 1st place on page 1!
                    [Sports related]🥇
                  </h6>
                  <div class="h6 font-weight-300">
                    <p class="ni location_pin mr-2">
                      It is a person who has benefited from the sports information community that has the highest competition rate in Korea.
                      <br>
                      🐱&zwj;🏍 Existing 3 pages -&gt; After work, I saw a dramatic synergistic effect as the key keyword ranked first.
                    </p>
                  </div>
                  <hr class="my-4">
                  <a href="#portfolio1" class="btn btn-theme" data-toggle="modal" data-target="#portfolio1">
                    View more
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-12 col-md-4 col-lg-4">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="#portfolio1" data-toggle="modal" data-target="#portfolio1">
                    <img src="assets/img/portfolio-1.png" alt="Portfolio - Sports" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400"><img src="assets/img/google-logo.png" class="img-reponsive img-logo" style="max-width: 1.5rem;">

                    : 1st place on page 1!
                    [Sports related]🥇


                  </h6>
                  <div class="h6 font-weight-300">
                    <p class="ni location_pin mr-2">


                      It is a person who has benefited from the sports information community that has the highest competition rate in Korea.
                      <br>


                      🐱&zwj;🏍 Existing 3 pages -&gt; After work, I saw a dramatic synergistic effect as the key keyword ranked first.

                    </p>
                  </div>
                  <hr class="my-4">
                  <a href="#portfolio1" class="btn btn-theme" data-toggle="modal" data-target="#portfolio1">
                    View more
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>


      </div>
    </div>
  </section>
</main>

<?php
include('inc/footer.php')
?>