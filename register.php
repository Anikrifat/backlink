<?php include('inc/header.php') ?>
<main class="main-content site-wrapper-reveal">
  <!--== Start Hero Area Wrapper ==-->
  <section class="page-title-area position-relative bgcolor-gray-silver bg-img p-0 pbn-5" data-bg-img="assets/img/slider/bg-1.jpg" style="background-image: url('assets/img/photos/page-title-02.jpg');max-width:100%;">
    <div class="bg-op-color"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
        <div class="page-title-content page-content-style4 text-center">
            <h2 class="title">
              Sign Up to Backlink </h2>
              <h5>Already have an account? <a href="register.php">Click Here For Sign In</a></h5>

          </div>
        </div>
      </div>
    </div>
    <div class="reg-login-contain">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-8 col-lg-6 mx-auto">
            <div class="card bg-white p-4 mb-2">
              <div class="card-body px-lg-6 py-lg-6">
                <div class="text-center text-muted mb-4">
                  <h5 class="bg-light p-3">
                    Sign Up
                  </h5>
                </div>
                <form role="form">
                  <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input class="form-control" placeholder="Enter username" type="username" id="username" name="username" minlength="4" maxlength="20">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                      </div>
                      <input class="form-control" placeholder="password" type="password" id="password" name="password" minlength="8" maxlength="20">
                    </div>
                  </div>
                  <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                      </div>
                      <input class="form-control" placeholder="e-mail" type="email" id="email" name="email">
                    </div>
                  </div>
                  <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      </div>
                      <input class="form-control" placeholder="Cell Phone" type="tel" id="tel" name="tel" maxlength="11">
                    </div>
                  </div>
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-globe"></i></span>
                      </div>
                      <input class="form-control" placeholder="Enter website URL" type="website" id="website" name="website" minlength="4" maxlength="20">
                    </div>

                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-telegram"></i></span>
                      </div>
                      <input class="form-control" placeholder="Enter Telegram Username" type="telegram" id="telegram" name="telegram" minlength="4" maxlength="20">
                    </div>
                    
                  <input type="hidden" id="g-recaptcha" name="g-recaptcha" value="03AGdBq25e3wShVpc4Iv0C2qHQBXRgdVbv-YSsq67uxl4GCxhC_xOD2fYlmbo1-fiaHH2cQx_fRHd05oxI2nPEct39mp1O7-eZgj252js2QXLmaCxpY9yopQ-J7BPS0q3MCKnqPQY05iDf6Zquq88ZkfKtyZlwRzetfypLF2tXnLADV0_tw74UiZvq6WkmEb-nbxMcv6U5odobppcatYQBnS_XilEqMYQCd1akW6HC6SGWfUGuZdZzaZc1bNoyi5O4RksSQEaGcOmSYOu75teET7fpSlgI4HatVnVYXEzG67ZP-QIeL8s09Zu8J11hSY_jWLMCAr7nYBZxUfM8msCsmLO7upTVuatc4YYeQWD3y_CjZAPe8zIDDIQB-LA09DQMTsaOzAVvnLKs5PQdSR4yB1WbY91w1jtZa86QqJcQtej_Op5bwvqVq1hTBBpPXRm8LB2X-SqA-UoN">
                  <div class="text-center">
                    <button type="submit" class="btn btn-success text-light my-4">
                   Sign Up
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<?php include('inc/footer.php') ?>