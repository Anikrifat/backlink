<?php
include('inc/header.php')
?>

<main class="main-content site-wrapper-reveal">
  <!--== Start Page Title Area ==-->
  <section class="page-title-area bgcolor-gray-silver bg-img p-0" data-bg-img="assets/img/photos/page-title-02.jpg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-title-content page-content-style4 text-center">
            <h2 class="title">FAQs</h2>
            <div class="bread-crumbs"><a href="index-2.html">Home /</a> FAQs</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Page Title Area ==-->

  <!--== Start Divider Area ==-->
  <section class="faq-area">
    <div class="container pt-160 pt-md-100 pb-120 pb-md-70">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-30 pb-2 md-mb-0">
            <h2 class="title">Frequently Asked Questions</h2>
          </div>
          <div class="accordian-content">
            <div class="accordion" id="accordionStyle">

              <div class="accordion-item">
                <div class="accordian-heading" id="headingOne">
                  <button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    What is the influence of SEO errors on the page ranking?
                    <i class="icon icon-plus icon_plus"></i>
                    <i class="icon icon-minus icon_minus-06"></i>
                  </button>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionStyle">
                  <div class="accordian-content">
                    Proper technical on page optimization is one of the important signals for search engine robots about the quality of your site. With optimizing the page based on recommendations of our website analyzer you can: Increase the amount of organic traffic by correctly filling the title, description, alt tags and check redirects. Improve the seo ranking positions of the site in the search engines by increasing the speed of the site and eliminating broken links. Eliminate the major site’s vulnerabilities. As a result, you won’t become a victim of unfair competition.
                  </div>
                </div>
              </div>

              <div class="accordion-item">
                <div class="accordian-heading" id="headingTwo">
                  <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    How to use SEO analyzer correctly?
                    <i class="icon icon-plus icon_plus"></i>
                    <i class="icon icon-minus icon_minus-06"></i>
                  </button>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionStyle">
                  <div class="accordian-content">
                    Proper technical on page optimization is one of the important signals for search engine robots about the quality of your site. With optimizing the page based on recommendations of our website analyzer you can: Increase the amount of organic traffic by correctly filling the title, description, alt tags and check redirects. Improve the seo ranking positions of the site in the search engines by increasing the speed of the site and eliminating broken links. Eliminate the major site’s vulnerabilities. As a result, you won’t become a victim of unfair competition.
                  </div>
                </div>
              </div>

              <div class="accordion-item">
                <div class="accordian-heading" id="headingThree">
                  <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    What does the page SEO score mean in Sitechecker?
                    <i class="icon icon-plus icon_plus"></i>
                    <i class="icon icon-minus icon_minus-06"></i>
                  </button>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionStyle">
                  <div class="accordian-content">
                    Proper technical on page optimization is one of the important signals for search engine robots about the quality of your site. With optimizing the page based on recommendations of our website analyzer you can: Increase the amount of organic traffic by correctly filling the title, description, alt tags and check redirects. Improve the seo ranking positions of the site in the search engines by increasing the speed of the site and eliminating broken links. Eliminate the major site’s vulnerabilities. As a result, you won’t become a victim of unfair competition.
                  </div>
                </div>
              </div>

              <div class="accordion-item">
                <div class="accordian-heading" id="headingFour">
                  <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    How to work with the results of website scan?
                    <i class="icon icon-plus icon_plus"></i>
                    <i class="icon icon-minus icon_minus-06"></i>
                  </button>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionStyle">
                  <div class="accordian-content">
                    Proper technical on page optimization is one of the important signals for search engine robots about the quality of your site. With optimizing the page based on recommendations of our website analyzer you can: Increase the amount of organic traffic by correctly filling the title, description, alt tags and check redirects. Improve the seo ranking positions of the site in the search engines by increasing the speed of the site and eliminating broken links. Eliminate the major site’s vulnerabilities. As a result, you won’t become a victim of unfair competition.
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Divider Area ==-->

  <!--== Start Contact Form Area Wrapper ==-->
  <section class="faq-form-area contact-form-area bgcolor-gray">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-80 pb-1">
            <h2 class="title mb-0">Submit Your Question</h2>
          </div>
          <div class="contact-form-content">
            <form id="contact-form" action="https://htmldemo.hasthemes.com/boseo/boseo/php/mail.php" method="post">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_name" placeholder="Name*">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input class="form-control" type="email" name="con_email" placeholder="Email*">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input class="form-control" type="text" name="con_subject" placeholder="Subject*">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea name="con_message" placeholder="Your Question*"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group mb-0">
                    <button class="btn btn-theme btn-block" type="submit">Send Message</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!-- Message Notification -->
          <div class="form-message"></div>
        </div>
      </div>
    </div>
    <div class="line-hr pt-0 pb-170 pb-md-100"></div>
  </section>
  <!--== End Contact Form Area Wrapper ==-->
</main>

<?php
include('inc/footer.php')
?>