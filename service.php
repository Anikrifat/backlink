<?php
include('inc/header.php')
?>
<main class="main-content site-wrapper-reveal bgcolor-gray">

  <div class="container py-5  py-5 mt-5 mt-md-0">
    <div class="row">
      <div class="col-lg-6 col-6 text-center">
        <a href="service.php" class="btn btn-bbb btn-block active">Service Intuduction</a>
      </div>
      <div class="col-lg-6 col-6 text-center">
        <a href="product.php" class="btn btn-bbb btn-block">Product Intuduction</a>
      </div>
    </div>
  </div>

  <div class="container-fluid py-3 text-center">
    <h1 class="font-weight-400">
      Backlink Pro Service Features
    </h1>
    <h6 class="font-weight-300 mx-auto">
      Backlink Pro's continuous backlink work builds awareness, so it
      can be exposed up to the final page in the search results of the portal site.
    </h6>
  </div>

  <div class="container-fluid">
    <div class="row justify-content-md-center pt-4 pb-4 text-center">
      <div class="col-xl-4">
        <div class="icon-xl icon-shape bg-success text-white shadow">
          <i class="fas fa-check-bold"></i>
        </div>
        <h2 class="display-4 nanums-3 mt-3" style="color:#000000;">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">easy service</font>
          </font>
        </h2>
        <p class="nanums-3 text-center mt-2 mb-5">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              There is no need </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              for </font>
            <font style="vertical-align: inherit;">complicated settings, </font>
            <font style="vertical-align: inherit;">just apply.</font>
          </font><br>
        </p>
      </div>
      <div class="col-xl-4">
        <div class="icon-xl icon-shape bg-primary text-white shadow">
          <i class="fas fa-chart-bar"></i>
        </div>
        <h2 class="display-4 nanums-3 mt-3" style="color:#000000;">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">Report provided</font>
          </font>
        </h2>
        <p class="nanums-3 text-center mt-2 mb-5">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              By providing a report, </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              it is possible to check </font>
            <font style="vertical-align: inherit;">the </font>
            <font style="vertical-align: inherit;">actual work result.</font>
          </font><br>
        </p>
      </div>
      <div class="col-xl-4">
        <div class="icon-xl icon-shape bg-warning text-white shadow">
          <i class="fas fa-settings-gear-65"></i>
        </div>
        <h2 class="display-4 nanums-3 mt-3" style="color:#000000;">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">steady update</font>
          </font>
        </h2>
        <p class="nanums-3 text-center mt-2 mb-5">
          <font style="vertical-align: inherit;"></font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              Backlink products are constantly updated according to the </font>
            <font style="vertical-align: inherit;">
              portal site algorithm.</font>
          </font><br>
        </p>
      </div>
    </div>
  </div>

  <div class="container-fluid bg-white">

    <div class="row justify-content-md-center pt-4 pb-4">
      <div class="col-xl-4 align-self-center">
        <h2 class="display-3 nanums-2"><strong class="text-purple">Google
          </strong>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;"> top exposure is </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">global backlink</font>
          </font>
        </h2>
        <p class="nanums-3">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              Google is heavily influenced by global site backlinks. </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              Global backlink products are available for Google top exposure.</font>
          </font><br>
        </p>
      </div>
      <div class="col-xl-4 text-center">
        <img src="assets/img/googlebacklinks.png" alt="google backlink" class="img-thumbnail" style="width:600px;">
      </div>
    </div>
  </div>

  <div class="container-fluid">

    <div class="row justify-content-md-center pt-4 pb-4">
      <div class="col-xl-4 text-center">
        <img src="assets/img/naverbacklinks.png" alt="Naver Backlink" class="img-thumbnail" style="width:600px;">
      </div>
      <div class="col-xl-4 align-self-center text-right">
        <h2 class="display-3 nanums-2"><strong class="text-success">
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">N The</font>
            </font>
          </strong>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;"> top exposure on </font><strong class="text-success">
              <font style="vertical-align: inherit;">domestic portals</font>
            </strong>
            <font style="vertical-align: inherit;"> is </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">domestic backlinks.</font>
          </font>
        </h2>
        <p class="nanums-3">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              Higher exposure on domestic portals </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              can increase </font>
            <font style="vertical-align: inherit;">the </font>
            <font style="vertical-align: inherit;">ranking </font>
            <font style="vertical-align: inherit;">through domestic-only domestic backlinks </font>
            <font style="vertical-align: inherit;">.</font>
          </font><br>
        </p>
      </div>
    </div>
  </div>

  <div class="container-fluid bg-white">

    <div class="row">
      <div class="col-xl-12 text-center mt-5 mb-5">
        <h2 class="nanums-2" style="color:#fb6340;">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">Worrying only slows down your ranking.</font>
          </font>
        </h2>
        <p class="text-center text-default mt-0 mb-5 nanums-3">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              If you would like a detailed explanation of each backlink before applying for backlink work, </font>
          </font><br>
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              please click the link below.</font>
          </font><br>
        </p>
        <a href="/product.php" class="btn btn-default">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">Product information</font>
              </font>
            </font>
          </font>
        </a>
      </div>
    </div>
  </div>

</main>

<?php
include('inc/footer.php')
?>