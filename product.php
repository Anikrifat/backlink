<?php
include('inc/header.php')
?>
<main class="main-content site-wrapper-reveal bgcolor-gray">
  <div class="container py-5 mt-5 mt-md-0">
    <div class="row">
      <div class="col-lg-6 col-6 text-center">
        <a href="service.php" class="btn btn-bbb btn-block">Service Intuduction</a>
      </div>
      <div class="col-lg-6 col-6 text-center">
        <a href="product.php" class="btn btn-bbb btn-block active">Product Intuduction</a>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">

      <div class="col-xl-4 order-xl-1">
        <div class="card card-profile shadow">
          <div class="card-body pt-0 pt-md-4">
            <div class="row">

              <div class="nav-wrapper col-12">
                <ul class="nav nav-pills nav-fill flex-column" id="tabs-icons-text" role="tablist">
                  <li class="nav-item text-left">
                    <a class="nav-link mb-sm-3 mb-md-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="false">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Premium Blended Backlink Campaign (with a lot of high domain score backlinks) [Points: -50,000]</font>
                      </font>
                    </a>
                  </li>
                  <li class="nav-item text-left">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;" class="">Premium Blended Backlink Campaign (Normal) [Points: -30,000]</font>
                      </font>
                    </a>
                  </li>
                  <li class="nav-item text-left">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Web 2.0 Blog (shared account) [Points: -450]</font>
                      </font>
                    </a>
                  </li>
                 
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-8 order-xl-2 mb-5 mb-xl-0">
        <div class="card bg-secondary shadow">
          <div class="card-header bg-white border-0">
            <div class="row align-items-center">
              <div class="col-8">
                <h3 class="mb-0">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">service</font>
                  </font>
                </h3>
              </div>
              <div class="col-4 text-right">
                <span class="badge badge-primary">SERVICE</span>
              </div>
            </div>
          </div>
          <div class="card-body bg-light">

            <div class="tab-content" id="myTabContent">
              <div class="text-left tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                <h3>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Premium Blended Backlink Campaign (with a lot of high domain score backlinks) [Points: -50,000] </font>
                  </font>
                </h3>
                <hr class="my-4">
                <div class="row">
                  <div class="col-xl-8 col-md-12 mb-5">
                    <p>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">This is an integrated backlink campaign that applies the latest algorithm for those who are difficult to apply for backlinks. </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">This product contains a large amount of backlinks with high domain scores. Backlinks of </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">various structures are performed and consist of about 500-700 backlinks.</font>
                      </font><br>
                    </p>
                    <hr class="my-4">
                    <ul style="list-style-type: none;">
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Includes backlinks to top recognizable websites on a domain score between 30 and 100.</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Tier 1 (High Quality Backlink) -&gt; Tier 2 (Quantitative Backlink) Configuration</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Profile structure &amp; body content insertion method</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Most do-follow backlinks</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Backlink keyword can be set</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Provides detailed reports on backlinks worked on</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Usually within 24 hours</font>
                        </font>
                      </li>
                    </ul>
                  </div>
                  <div class="col-xl-4 col-md-12 mb-5">
                    <div class="table-responsive">
                      <table class="table align-items-center table-flush">
                        <tbody>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Recommended Tier</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> Tier 1-3 mix </font>
                              </font>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Minimum order quantity</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> One </font>
                              </font>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-left tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                <h3>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Premium Blended Backlink Campaign (Normal) [Points: -30,000] </font>
                  </font>
                </h3>
                <hr class="my-4">
                <div class="row">
                  <div class="col-xl-8 col-md-12 mb-5">
                    <p>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">This is an integrated backlink campaign for those who are difficult to apply for backlinks. </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">This product contains a large amount of backlinks with high domain scores. Backlinks of </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">various structures are performed and consist of about 500-700 backlinks.</font>
                      </font><br>
                    </p>
                    <hr class="my-4">
                    <ul style="list-style-type: none;">
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Includes backlinks to top recognizable websites on a domain score between 30 and 100.</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Tier 1 (High Quality Backlink) -&gt; Tier 2 (Quantitative Backlink) Configuration</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Profile structure &amp; body content insertion method</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Most do-follow backlinks</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Backlink keyword can be set</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Provides detailed reports on backlinks worked on</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Usually within 24 hours</font>
                        </font>
                      </li>
                    </ul>
                  </div>
                  <div class="col-xl-4 col-md-12 mb-5">
                    <div class="table-responsive">
                      <table class="table align-items-center table-flush">
                        <tbody>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Recommended Tier</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> Tier 1-3 mix </font>
                              </font>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Minimum order quantity</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> One </font>
                              </font>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-left tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                <h3>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Web 2.0 Blog (shared account) [Points: -450] </font>
                  </font>
                </h3>
                <hr class="my-4">
                <div class="row">
                  <div class="col-xl-8 col-md-12 mb-5">
                    <p>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">We work on backlinks to famous blog websites such as wordpress, tumblr, and blog.com overseas. </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Backlinks to web 2.0 websites in the form of a community where people interact interactively </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">can be more effective in search results. </font>
                      </font><br>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">[Web 2.0 Blog (Shared Account)] The backlink product will be of great help in website search rankings.</font>
                      </font>
                    </p>
                    <hr class="my-4">
                    <ul style="list-style-type: none;">
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">It consists of backlinks to the top recognizable websites with a domain score between 30 and 100</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">High-quality backlinks to famous overseas blog websites (shared accounts)</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Profile structure &amp; body content insertion method</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Most do-follow backlinks</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Backlink keyword can be set</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Provides detailed reports on backlinks worked on</font>
                        </font>
                      </li>
                      <li><i class="ni ni-check-bold"></i>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Usually within 24 hours</font>
                        </font>
                      </li>
                    </ul>
                  </div>
                  <div class="col-xl-4 col-md-12 mb-5">
                    <div class="table-responsive">
                      <table class="table align-items-center table-flush">
                        <tbody>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Recommended Tier</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> Tier 1 </font>
                              </font>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">
                              <div class="media align-items-center">
                                <div class="media-body"> <span class="mb-0 text-sm">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">Minimum order quantity</font>
                                    </font>
                                  </span> </div>
                              </div>
                            </th>
                            <td>
                              <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> 10 things </font>
                              </font>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php
include('inc/footer.php');
?>