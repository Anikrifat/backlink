<?php include('inc/header.php') ?>
<main class="main-content site-wrapper-reveal bgcolor-gray">
<div class="container py-5 mt-5 pt-md-0">
  <div class="row">

    <div class="col-xl-12 order-xl-1 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">How to use</font>
                </font>
              </h3>
            </div>
            <div class="col-4 text-right">
              <span class="badge badge-primary">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">GUIDE</font>
                </font>
              </span>
            </div>
          </div>
        </div>
        <div class="card-body bg-light">

          <div class="container-fluid bg-white">

            <div class="row">
              <div class="col-xl-12 text-center mt-3 mb-5">
                <h1 class="display-3">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">1. Charging</font>
                  </font>
                </h1>
                <p>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Before applying for the backlink campaign, you must proceed with the points recharge. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Please access the recharge menu and apply for points recharge. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Point recharge In case of direct deposit, the charge will be approved within 24 hours.</font>
                  </font><br>
                </p>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/fund1-anymation.gif" alt="How to use backlink - recharge" class="img-thumbnail">
                </div>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/fund2-anymation.gif" alt="How to use backlink - recharge" class="img-thumbnail">
                </div>
              </div>
            </div>
          </div>


          <div class="container-fluid">

            <div class="row">
              <div class="col-xl-12 text-center mt-3 mb-5">
                <h1 class="display-3">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">2. Request a backlink</font>
                  </font>
                </h1>
                <p>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      When the point charge is confirmed, access the backlink application menu. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Please access the recharge menu and apply for points recharge. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Just apply for the backlink campaign you want.</font>
                  </font><br>
                </p>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/order1-anymation.gif" alt="How to use backlink - apply for backlink" class="img-thumbnail">
                </div>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/order2-anymation.gif" alt="How to use backlink - apply for backlink" class="img-thumbnail">
                </div>
              </div>
            </div>
          </div>


          <div class="container-fluid bg-white">

            <div class="row">
              <div class="col-xl-12 text-center mt-3 mb-5">
                <h1 class="display-3">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">3. Check work</font>
                  </font>
                </h1>
                <p>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      After completing the work, you can check the work history through the report page. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      You can check each order, and after completion, </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      you can download the </font>
                    <font style="vertical-align: inherit;">report in text or Excel format </font>
                    <font style="vertical-align: inherit;">.</font>
                  </font><br>
                </p>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/reports1-anymation.gif" alt="How to use backlinks - check work" class="img-thumbnail">
                </div>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/reports2.png" alt="How to use backlinks - check work" class="img-thumbnail">
                </div>
              </div>
            </div>
          </div>


          <div class="container-fluid">

            <div class="row">
              <div class="col-xl-12 text-center mt-3 mb-5">
                <h1 class="display-3">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">4. Rank reflection</font>
                  </font>
                </h1>
                <p>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      It takes an average of 6-12 weeks to reflect the search engine rankings at the time of completion of the work. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      Due to the nature of the backlink work, ranking is not guaranteed. </font>
                  </font><br>
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                      It is more effective if you focus on internal search engine optimization and content management along with backlink work.</font>
                  </font><br>
                </p>
                <div class="col-xl-12 text-center mt-5 mb-5">
                  <img src="assets/img/check-anymation.gif" alt="Backlink Usage - Rank Reflection" class="img-thumbnail">
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</main>
<?php include('inc/footer.php') ?>