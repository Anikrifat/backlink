<?php
include('inc/header.php')
?>

<main class="main-content site-wrapper-reveal">
  <!--== Start Page Title Area ==-->
  <section class="page-title-area pb-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="page-title-content pt-20 pb-3">
            <div class="bread-crumbs"><a href="index-2.html">Home /</a> About Us</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Page Title Area ==-->

  <!--== Start Divider Area ==-->
  <section class="divider-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-30 pb-2">
            <div class="row">
              <div class="col-lg-8 col-md-10 m-auto">
                <h2 class="title">We Are Constantly Ranked as Best SEO Services Company</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-10 m-auto">
                <div class="desc">
                  <p>Whether you do a search for “London seo”, “London web or a variety of other highly competitive terms in our industry you will find that we completely dominate our competitors at our own game. Further, you will see we are the top rated firm with 67 reviews on Google giving us 4.8 stars.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="thumb">
            <img class="w-100" src="assets/img/about/v06.png" alt="Boseo-HasTech">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Divider Area ==-->

  <!--== Start Service Area ==-->
  <section class="service-area service-agency-area bgcolor-gray-silver pt-140 pt-md-60 pb-120 pb-md-70">
    <div class="container">
      <div class="row pt-2">
        <div class="col-md-11 col-lg-5 col-xl-4">
          <div class="section-title mt-30">
            <h5>Services</h5>
            <h2 class="title pr-30">Services We Provide Special For You</h2>
            <div class="desc pr-20">
              <p class="mb-md-50">BOSEO is a full-service digital marketing agency with a long history of delivering great results for our clients. We take an individualized approach to every customer project.</p>
              <a href="#/" class="btn btn-theme">All Services</a>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-7 offset-lg-0 col-xl-6 offset-xl-1">
          <div class="row icon-box-style1">
            <div class="col-lg-6">
              <div class="icon-box-item item-one">
                <div class="icon-box">
                  <img class="icon-img" src="assets/img/icons/01.png" alt="Icon">
                </div>
                <div class="content">
                  <h4><a href="#/">Organic Search</a></h4>
                  <p>Stop selling. Start helping. Utilize SEO to capture early stage awareness</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="icon-box-item item-two">
                <div class="icon-box">
                  <img class="icon-img" src="assets/img/icons/02.png" alt="Icon">
                </div>
                <div class="content">
                  <h4><a href="#/">Link Building</a></h4>
                  <p>Our full-service link building company specializes in custom link building</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="icon-box-item item-three">
                <div class="icon-box">
                  <img class="icon-img" src="assets/img/icons/03.png" alt="Icon">
                </div>
                <div class="content">
                  <h4><a href="#/">Paid Search</a></h4>
                  <p>Search engine marketing (SEM, which can include SEO), pay-per-click (PPC)</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="icon-box-item item-four">
                <div class="icon-box">
                  <img class="icon-img" src="assets/img/icons/04.png" alt="Icon">
                </div>
                <div class="content">
                  <h4><a href="#/">SEO Auditing</a></h4>
                  <p>If you haven't recently audit website, we recommend an SEO audit </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Service Area ==-->

  <!--== Start Working Process Area ==-->
  <section class="working-process-area">
    <div class="container pt-160 pt-md-100 pb-150 pb-md-100">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-80 mb-md-50">
            <h5>How It Works</h5>
            <h2 class="title mb-0">Our Process</h2>
          </div>
        </div>
      </div>
      <div class="working-process-content">
        <div class="row">
          <div class="col-lg-12">
            <!--== Start Working Process Nav ==-->
            <ul class="working-process-nav nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" id="keyword-research-tab" data-toggle="tab" href="#keyword-research-one" role="tab" aria-controls="keyword-research-one" aria-selected="true">
                  <span class="nav-link-content"><img src="assets/img/icons/25.png" alt="Image-Icon">
                    Keyword <br>Research</span>
                  <span class="bg-white-shape"></span>
                  <span class="bg-line-bottom"></span>
                </a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" id="analyze-website-tab" data-toggle="tab" href="#analyze-website-two" role="tab" aria-controls="analyze-website-two" aria-selected="false">
                  <span class="nav-link-content"><img src="assets/img/icons/26.png" alt="Image-Icon">
                    Analyze <br>Website</span>
                  <span class="bg-white-shape"></span>
                  <span class="bg-line-bottom"></span>
                </a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" id="link-building-tab" data-toggle="tab" href="#link-building-three" role="tab" aria-controls="link-building-three" aria-selected="false">
                  <span class="nav-link-content"><img src="assets/img/icons/27.png" alt="Image-Icon">
                    Link <br>Building</span>
                  <span class="bg-white-shape"></span>
                  <span class="bg-line-bottom"></span>
                </a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" id="report-results-tab" data-toggle="tab" href="#report-results-four" role="tab" aria-controls="report-results-four" aria-selected="false">
                  <span class="nav-link-content"><img src="assets/img/icons/28.png" alt="Image-Icon">
                    Report <br>Results</span>
                  <span class="bg-white-shape"></span>
                  <span class="bg-line-bottom"></span>
                </a>
              </li>
            </ul>
            <!--== End Working Process Nav ==-->

            <!--== Start Working Process Content ==-->
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="keyword-research-one" role="tabpanel" aria-labelledby="keyword-research-tab">
                <div class="row">
                  <div class="col-xl-5 col-lg-6">
                    <h4 class="title">Keyword Research is a Practice Search Engine Optimization</h4>
                    <p>Search engine optimization professional’s research keywords, which they use to achieve better rankings in search engines. Our consultants will start by getting to know you and your business. We’ll make every effort to get an understanding of your objectives, products and production.</p>
                    <ul>
                      <li><i class="icon_box-checked"></i>We always ensure that we have selected keywords that have the least number of competitors.</li>
                      <li><i class="icon_box-checked"></i>We’ll also analyze search data for your niche</li>
                      <li><i class="icon_box-checked"></i>We’ll find the best keywords that we can focus the campaign around.</li>
                    </ul>
                  </div>
                  <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="thumb">
                      <img src="assets/img/photos/vector/working-process-01.png" alt="Boseo-HasTech">
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="analyze-website-two" role="tabpanel" aria-labelledby="analyze-website-tab">
                <div class="row">
                  <div class="col-xl-5 col-lg-6">
                    <h4 class="title">Analyze Website is a Practice Search Engine Optimization</h4>
                    <p>Search engine optimization professional’s research keywords, which they use to achieve better rankings in search engines. Our consultants will start by getting to know you and your business. We’ll make every effort to get an understanding of your objectives, products and production.</p>
                    <ul>
                      <li><i class="icon_box-checked"></i>We always ensure that we have selected keywords that have the least number of competitors.</li>
                      <li><i class="icon_box-checked"></i>We’ll also analyze search data for your niche</li>
                      <li><i class="icon_box-checked"></i>We’ll find the best keywords that we can focus the campaign around.</li>
                    </ul>
                  </div>
                  <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="thumb">
                      <img src="assets/img/photos/vector/working-process-01.png" alt="Boseo-HasTech">
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="link-building-three" role="tabpanel" aria-labelledby="link-building-tab">
                <div class="row">
                  <div class="col-xl-5 col-lg-6">
                    <h4 class="title">Link Building is a Practice Search Engine Optimization</h4>
                    <p>Search engine optimization professional’s research keywords, which they use to achieve better rankings in search engines. Our consultants will start by getting to know you and your business. We’ll make every effort to get an understanding of your objectives, products and production.</p>
                    <ul>
                      <li><i class="icon_box-checked"></i>We always ensure that we have selected keywords that have the least number of competitors.</li>
                      <li><i class="icon_box-checked"></i>We’ll also analyze search data for your niche</li>
                      <li><i class="icon_box-checked"></i>We’ll find the best keywords that we can focus the campaign around.</li>
                    </ul>
                  </div>
                  <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="thumb">
                      <img src="assets/img/photos/vector/working-process-01.png" alt="Boseo-HasTech">
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="report-results-four" role="tabpanel" aria-labelledby="report-results-tab">
                <div class="row">
                  <div class="col-xl-5 col-lg-6">
                    <h4 class="title">Report Results is a Practice Search Engine Optimization</h4>
                    <p>Search engine optimization professional’s research keywords, which they use to achieve better rankings in search engines. Our consultants will start by getting to know you and your business. We’ll make every effort to get an understanding of your objectives, products and production.</p>
                    <ul>
                      <li><i class="icon_box-checked"></i>We always ensure that we have selected keywords that have the least number of competitors.</li>
                      <li><i class="icon_box-checked"></i>We’ll also analyze search data for your niche</li>
                      <li><i class="icon_box-checked"></i>We’ll find the best keywords that we can focus the campaign around.</li>
                    </ul>
                  </div>
                  <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="thumb">
                      <img src="assets/img/photos/vector/working-process-01.png" alt="Boseo-HasTech">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--== End Working Process Content ==-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Working Process Area ==-->

  <!--== Start Testimonial Area ==-->
  <section class="testimonial-area bgcolor-gray">
    <div class="container pt-150 pt-md-100 pb-150 pb-md-170">
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="section-title mb-md-50">
            <h5>Testimonials</h5>
            <h2 class="title">Trusted From My Clients</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="testimonial-slider">
            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“Kelly Chandler is definitely an expert in digital marketing! She is always doing a great job and takes time to explain her thoughts and her process. She doesn't hesitate to share new ideas and be proactive all the time, which is great when you want to grow your business. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/01.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Chris Carroll</h4>
                    <p class="designation">CEO Founder at Google Inc</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We’ve been with higher visibility for about two months now and with the help of our account manager Lauren Moscato, we’ve been able to improve our website and rankings dramatically. We will be continuing business with them and are excited to see what’s to come. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/02.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Julia Steve</h4>
                    <p class="designation">Manager at Spotify LLC</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“A company that delivers results from the moment you start. We had been managing our own accounts and our SEO had only been a small part of our efforts. With Higher Visibility a weight was lifted off our shoulders on the campaigns they now manage and our SEO ranking is growing daily.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/03.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Anna Houston</h4>
                    <p class="designation">Senior Executive at Dropbox</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“I wanted to take a moment and let you know how happy we are. You have show great knowledge in regards to SEO best practices as well as optimization strategy and management of the BOSEO.com account. Most importantly, you have obtained results, and that is an undeniable measure.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/04.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Ed Dodd</h4>
                    <p class="designation">CEO at Ridegear.com</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“MyResort.com has quadrupled their visitors thanks to BOSEO. They have adjusted our pages, and given us recommendations to make ourselves. They have made us one of the top timeshare companies in the world. Their account manager for us has been very responsive to our needs.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/05.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Mike Barton</h4>
                    <p class="designation">Owner at MyResort.com</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We are a very satisfied client of BOSEO. Our traffic has increased substantially as well as a significant increase in the quality of our leads. Their efforts have contributed to a 40% increase in our sales. Garry and his team have been great to work with...the bottom line is that BOSEO. delivers results”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/06.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Mark Denham</h4>
                    <p class="designation">President at 247 Workspace</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We’ve been with higher visibility for about two months now and with the help of our account manager Lauren Moscato, we’ve been able to improve our website and rankings dramatically. We will be continuing business with them and are excited to see what’s to come. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/02.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Julia Steve</h4>
                    <p class="designation">Manager at Spotify LLC</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“Kelly Chandler is definitely an expert in digital marketing! She is always doing a great job and takes time to explain her thoughts and her process. She doesn't hesitate to share new ideas and be proactive all the time, which is great when you want to grow your business. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/01.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Chris Carroll</h4>
                    <p class="designation">CEO Founder at Google Inc</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Testimonial Area ==-->

  <!--== Start Brand Logo Area ==-->
  <section class="brand-logo-area bgcolor-gray">
    <div class="container pb-110 pb-md-70">
      <div class="row">
        <div class="col-lg-12">
          <div class="brand-logo-content-area">
            <div class="brand-list brand-logo-slider owl-carousel owl-theme">
              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/1.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/2.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/3.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/4.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/5.png" alt="Boseo-Logo" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== Start Brand Logo Area ==-->

  <!--== Start Team Area ==-->
  <section class="team-area">
    <div class="container pt-150 pt-sm-100 pb-120 pb-md-70">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title mb-50">
            <h5>Trusted Experts</h5>
            <h2 class="title mb-0">Meet Our Team</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="team-col3-carousel owl-carousel owl-theme owl-nav4">
            <div class="item">
              <div class="team-member">
                <div class="thumb">
                  <img src="assets/img/team/01.jpg" alt="Boseo-HasTech">
                </div>
                <div class="content">
                  <div class="member-info">
                    <h4 class="name"><a href="#/">Seth Norwood</a></h4>
                    <p class="designation">Lead Analyst</p>
                  </div>
                </div>
                <a class="icon team-btn-active" href="#/">
                  <img src="assets/img/icons/eye.png" alt="Icon-Image">
                </a>
                <div class="hover-content">
                  <div class="member-info">
                    <a class="icon team-btn-close" href="#/">
                      <i class="lnr lnr-cross"></i>
                    </a>
                    <h4 class="name"><a href="#/">Seth Norwood</a></h4>
                    <p class="designation">Lead Analyst</p>
                    <p class="desc">Seth has over 14 years of direct, hands-on internet marketing experi- ence. He has been the SEO Manager for a top interactive agency as well as several large e-commerce sites</p>
                    <div class="social-icons">
                      <a href="#"><i class="social social_facebook"></i></a>
                      <a href="#"><i class="social social_twitter"></i></a>
                      <a href="#"><i class="social social_vimeo"></i></a>
                      <a href="#"><i class="social social_share"></i></a>
                      <a href="#"><i class="social social_skype"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="team-member">
                <div class="thumb">
                  <img src="assets/img/team/02.jpg" alt="Boseo-HasTech">
                </div>
                <div class="content">
                  <div class="member-info">
                    <h4 class="name"><a href="#/">Sabrina Brooks</a></h4>
                    <p class="designation">SEO Strategist</p>
                  </div>
                </div>
                <a class="icon team-btn-active" href="#/">
                  <img src="assets/img/icons/eye.png" alt="Icon-Image">
                </a>
                <div class="hover-content">
                  <div class="member-info">
                    <a class="icon team-btn-close" href="#/">
                      <i class="lnr lnr-cross"></i>
                    </a>
                    <h4 class="name"><a href="#/">Sabrina Brooks</a></h4>
                    <p class="designation">SEO Strategist</p>
                    <p class="desc">Seth has over 14 years of direct, hands-on internet marketing experi- ence. He has been the SEO Manager for a top interactive agency as well as several large e-commerce sites</p>
                    <div class="social-icons">
                      <a href="#"><i class="social social_facebook"></i></a>
                      <a href="#"><i class="social social_twitter"></i></a>
                      <a href="#"><i class="social social_vimeo"></i></a>
                      <a href="#"><i class="social social_share"></i></a>
                      <a href="#"><i class="social social_skype"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="team-member">
                <div class="thumb">
                  <img src="assets/img/team/03.jpg" alt="Boseo-HasTech">
                </div>
                <div class="content">
                  <div class="member-info">
                    <h4 class="name"><a href="#/">Darian Cloward</a></h4>
                    <p class="designation">Account Manager</p>
                  </div>
                </div>
                <a class="icon team-btn-active" href="#/">
                  <img src="assets/img/icons/eye.png" alt="Icon-Image">
                </a>
                <div class="hover-content">
                  <div class="member-info">
                    <a class="icon team-btn-close" href="#/">
                      <i class="lnr lnr-cross"></i>
                    </a>
                    <h4 class="name"><a href="#/">Darian Cloward</a></h4>
                    <p class="designation">Account Manager</p>
                    <p class="desc">Seth has over 14 years of direct, hands-on internet marketing experi- ence. He has been the SEO Manager for a top interactive agency as well as several large e-commerce sites</p>
                    <div class="social-icons">
                      <a href="#"><i class="social social_facebook"></i></a>
                      <a href="#"><i class="social social_twitter"></i></a>
                      <a href="#"><i class="social social_vimeo"></i></a>
                      <a href="#"><i class="social social_share"></i></a>
                      <a href="#"><i class="social social_skype"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="team-member">
                <div class="thumb">
                  <img src="assets/img/team/04.jpg" alt="Boseo-HasTech">
                </div>
                <div class="content">
                  <div class="member-info">
                    <h4 class="name"><a href="#/">James Baker</a></h4>
                    <p class="designation">SEO Engineer</p>
                  </div>
                </div>
                <a class="icon team-btn-active" href="#/">
                  <img src="assets/img/icons/eye.png" alt="Icon-Image">
                </a>
                <div class="hover-content">
                  <div class="member-info">
                    <a class="icon team-btn-close" href="#/">
                      <i class="lnr lnr-cross"></i>
                    </a>
                    <h4 class="name"><a href="#/">Darian Cloward</a></h4>
                    <p class="designation">Account Manager</p>
                    <p class="desc">Seth has over 14 years of direct, hands-on internet marketing experi- ence. He has been the SEO Manager for a top interactive agency as well as several large e-commerce sites</p>
                    <div class="social-icons">
                      <a href="#"><i class="social social_facebook"></i></a>
                      <a href="#"><i class="social social_twitter"></i></a>
                      <a href="#"><i class="social social_vimeo"></i></a>
                      <a href="#"><i class="social social_share"></i></a>
                      <a href="#"><i class="social social_skype"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Team Area ==-->
</main>
<?php
include('inc/footer.php')
?>