<?php
include('inc/header.php')
?>
<main class="main-content site-wrapper-reveal bgcolor-gray">
  <!--== Start Pricing Area Wrapper ==-->
  <section class="pricing-area">
    <div class="container-fluid pt-125 pt-md-100">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center mb-80 mb-md-50">
            <h5>Pricing Table</h5>
            <h2 class="title">Choose Your Best Plan</h2>
          </div>
        </div>
      </div>
      <div class="row justify-content-md-center">

        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/orderPerfect.php">
                    <img src="assets/img/orderPerfect.png" alt="Perfect Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Perfect Backlink Order </font>
                    </font><span class="badge badge-success">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">NEW</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We are working on artificial intelligence-based perfect backlinks to domestic sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 1-3 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">high ranking effect</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Rank Up Only</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Artificial intelligence (AI) based document creation</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">100% Do-Follow Backlink</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦160,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderPerfect.php" class="btn btn-primary">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">

                  <a href="/orderKorea.php">
                    <img src="assets/img/orderKorea.png" alt="Domestic Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Domestic Backlink Order </font>
                    </font><span class="badge badge-warning">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">UPDATE</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We work on backlinks to domestic community sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★ (Normal)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 2-4 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Intensive work on domestic portals</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Domestic portal ranking rise &amp; keyword exposure for the first time</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">domain score random</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">100% Do-Follow Backlink</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦99,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderKorea.php" class="btn btn-success">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/order.php">
                    <img src="assets/img/orderGlobal.png" alt="Global Backlink Thumbnail" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Global Backlink Order
                      </font>
                    </font>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We provide various types of backlinks to overseas sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Domestic portal: ★★★ (Normal)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Google: ★★★★ (recommended)</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Working period: 1-2 days</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Effective within 4-8 weeks on average</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">reasonable price</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i><strong>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Google Ranking Up &amp; Keywords First Exposure</font>
                        </font>
                      </strong></li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">High domain score </font>
                      </font><small>
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">(varies by product)</font>
                        </font>
                      </small>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">various backlinks</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦50,000~
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./order.php" class="btn btn-default">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-xl-3 mb-md-5">
          <div class="card card-profile shadow">
            <div class="card-body pt-0 pt-md-4">
              <div class="row">

                <div class="nav-wrapper col-12">
                  <a href="/orderFree.php">
                    <img src="assets/img/orderFree.png" alt="FREE backlink thumbnails" class="img-thumbnail">
                  </a>
                </div>

              </div>

              <div class="tab-content" id="myTabContent">
                <div class="text-left tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                  <h6 class="font-weight-400">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        FREE backlink order </font>
                    </font><span class="badge badge-success">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">NEW</font>
                      </font>
                    </span>
                  </h6>
                  <div class="h6 font-weight-300">
                    <i class="ni location_pin mr-2"></i>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">We are working on artificial intelligence-based perfect backlink samples on domestic sites.
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <ul style="list-style-type: none;" class="pl-1">
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Perfect Backlink Sample</font>
                      </font>
                    </li>
                    <li><i class="ni ni-check-bold"></i>
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">2 free works when registering as a new member</font>
                      </font>
                    </li>
                  </ul>
                  <div class="h6 font-weight-400 text-right">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">
                        Recommended amount of work ￦0
                      </font>
                    </font>
                  </div>
                  <hr class="my-4">
                  <a href="./orderPerfect.php?type=free" class="btn btn-secondary">
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Apply</font>
                    </font>
                  </a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Pricing Area Wrapper ==-->

  <!--== Start Testimonial Area ==-->
  <section class="testimonial-area bgcolor-gray">
    <div class="container pt-165 pt-md-120 pt-sm-70 pb-140">
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="section-title mb-90 mb-md-50">
            <h5>Testimonials</h5>
            <h2 class="title mb-0">Trusted From My Clients</h2>
          </div>
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-lg-12">
          <div class="testimonial-slider">
            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“Kelly Chandler is definitely an expert in digital marketing! She is always doing a great job and takes time to explain her thoughts and her process. She doesn't hesitate to share new ideas and be proactive all the time, which is great when you want to grow your business. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/01.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Chris Carroll</h4>
                    <p class="designation">CEO Founder at Google Inc</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We’ve been with higher visibility for about two months now and with the help of our account manager Lauren Moscato, we’ve been able to improve our website and rankings dramatically. We will be continuing business with them and are excited to see what’s to come. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/02.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Julia Steve</h4>
                    <p class="designation">Manager at Spotify LLC</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“A company that delivers results from the moment you start. We had been managing our own accounts and our SEO had only been a small part of our efforts. With Higher Visibility a weight was lifted off our shoulders on the campaigns they now manage and our SEO ranking is growing daily.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/03.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Anna Houston</h4>
                    <p class="designation">Senior Executive at Dropbox</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“I wanted to take a moment and let you know how happy we are. You have show great knowledge in regards to SEO best practices as well as optimization strategy and management of the BOSEO.com account. Most importantly, you have obtained results, and that is an undeniable measure.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/04.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Ed Dodd</h4>
                    <p class="designation">CEO at Ridegear.com</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“MyResort.com has quadrupled their visitors thanks to BOSEO. They have adjusted our pages, and given us recommendations to make ourselves. They have made us one of the top timeshare companies in the world. Their account manager for us has been very responsive to our needs.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/05.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Mike Barton</h4>
                    <p class="designation">Owner at MyResort.com</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We are a very satisfied client of BOSEO. Our traffic has increased substantially as well as a significant increase in the quality of our leads. Their efforts have contributed to a 40% increase in our sales. Garry and his team have been great to work with...the bottom line is that BOSEO. delivers results”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/06.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Mark Denham</h4>
                    <p class="designation">President at 247 Workspace</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>

            <div class="testimonial-carousel-item">
              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“We’ve been with higher visibility for about two months now and with the help of our account manager Lauren Moscato, we’ve been able to improve our website and rankings dramatically. We will be continuing business with them and are excited to see what’s to come. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/02.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Julia Steve</h4>
                    <p class="designation">Manager at Spotify LLC</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->

              <!--== Start Testimonial Item ==-->
              <div class="testimonial-item line-top">
                <div class="client-content">
                  <p>“Kelly Chandler is definitely an expert in digital marketing! She is always doing a great job and takes time to explain her thoughts and her process. She doesn't hesitate to share new ideas and be proactive all the time, which is great when you want to grow your business. Thank you very much.”</p>
                </div>
                <div class="client-info">
                  <div class="thumb">
                    <img src="assets/img/testimonial/01.jpg" alt="Image">
                  </div>
                  <div class="desc">
                    <h4 class="name">Chris Carroll</h4>
                    <p class="designation">CEO Founder at Google Inc</p>
                  </div>
                  <div class="icon-quote"><img src="assets/img/icons/right-quote.png" alt="Image"></div>
                </div>
              </div>
              <!--== End Testimonial Item ==-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== End Testimonial Area ==-->

  <!--== Start Brand Logo Area ==-->
  <section class="brand-logo-area bgcolor-gray">
    <div class="container pb-130 pb-md-70">
      <div class="row">
        <div class="col-lg-12">
          <div class="brand-logo-content-area">
            <div class="brand-list brand-logo-slider owl-carousel owl-theme">
              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/1.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/2.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/3.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/4.png" alt="Boseo-Logo" /></a>
              </div>

              <div class="brand-logo-item">
                <a href="#/"><img src="assets/img/brand-logo/5.png" alt="Boseo-Logo" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="line-hr m-0"></div>
  </section>
  <!--== Start Brand Logo Area ==-->
</main>

<?php
include('inc/footer.php')
?>