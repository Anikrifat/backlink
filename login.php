<?php include('inc/header.php') ?>
<main class="main-content site-wrapper-reveal">
  <!--== Start Hero Area Wrapper ==-->
  <section class="page-title-area position-relative bgcolor-gray-silver bg-img p-0 pbn-5" data-bg-img="assets/img/slider/bg-1.jpg" style="background-image: url('assets/img/photos/page-title-02.jpg');max-width:100%;">
    <div class="bg-op-color"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-title-content page-content-style4 text-center">
            <h2 class="title">
              Sign in to Backlink </h2>
              <h5>Not have an account> <a href="register.php">Click Here For Sign Up</a></h5>

          </div>
        </div>
      </div>
    </div>
    <div class="reg-login-contain">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-8 col-lg-6 mx-auto">
            <div class="card bg-white p-4 mb-2">
              <div class="card-body px-lg-6 py-lg-6">
              <div class="text-center text-muted mb-4">
                  <h5 class="bg-light p-3">
                    Sign In
                  </h5>
                </div>
                <form role="form">
                  <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input class="form-control" placeholder="Enter username" type="username" id="username" name="username" minlength="4" maxlength="20">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                      </div>
                      <input class="form-control" placeholder="password" type="password" id="password" name="password" minlength="8" maxlength="20">
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-success my-4">
                    Sign In
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<?php include('inc/footer.php') ?>