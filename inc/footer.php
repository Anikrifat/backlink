    <!--== Start Footer Area Wrapper ==-->
    <footer class="footer-area reveal-footer footer-style-two">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-xl-3">
            <div class="widget-item mt-0">
              <div class="about-widget">
                <a class="footer-logo" href="index-2.php">
                  Backlink
                </a>
              </div>
              <div class="widget-copyright">
                <p>© 2021. All Rights Reserved</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-xl-5">
            <div class="widget-item">
              <h4 class="widget-title title-style-two">Contact</h4>
              <ul class="widget-contact-info">
                <li class="info-address">258 Orchad St, London, United Kingdom</li>
                <li class="info-mail">sayhello@backlink.com</li>
                <li class="info-phone">(605) 230-5253</li>
              </ul>
            </div>
          </div>
          <div class="col-lg-5 col-xl-4">
            <div class="widget-item pl-40 md-pl-0">
              <nav class="widget-menu-wrap">
                <ul class="nav-menu nav-menu-three nav">
                  <li><a href="index-2.php">Home</a></li>
                  <li><a href="services.php">Services</a></li>
                  <li><a href="faq.php">FAQs</a></li>
                  <li><a href="blog.php">Blog</a></li>
                </ul>
              </nav>
              <div class="widget-social-icons social-icons-style-three">
                <a href="#"><i class="social social_facebook"></i></a>
                <a href="#"><i class="social social_twitter"></i></a>
                <a href="#"><i class="social social_pinterest"></i></a>
                <a href="#"><i class="social social_vimeo"></i></a>
                <a href="#"><i class="social social_share"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="layer-shape">
        <img class="layer-shape-two" src="assets/img/shape/foooter-shape-02.png" alt="backlink-Image">
        <img class="layer-shape-one" src="assets/img/shape/foooter-shape-01.png" alt="backlink-Image">
      </div>
    </footer>
    <!--== End Footer Area Wrapper ==-->

    <!--== Scroll Top Button ==-->
    <div class="scroll-to-top"><span class="arrow_up"></span></div>
<div class="mpbile-number bg-danger w-100 fixed-bottom text-center  text-white py-2"><p>013456679895</p></div>
    <!--== Start Side Menu ==-->
    <aside class="off-canvas-wrapper">
      <div class="off-canvas-inner">
        <div class="off-canvas-overlay"></div>
        <!-- Start Off Canvas Content Wrapper -->
        <div class="off-canvas-content">
          <!-- Off Canvas Header -->
          <div class="off-canvas-header">
            <div class="logo-area">
              <a href="index-2.php">Backlink</a>
            </div>
            <ul class="widget-language">
              <li><a class="active" href="#/">Eng</a></li>
              <li><a href="#/">Fre</a></li>
              <li><a href="#/">Ger</a></li>
            </ul>
            <div class="close-action">
              <button class="btn-close"><i class="lnr lnr-cross"></i></button>
            </div>
          </div>

          <div class="off-canvas-item">
            <!-- Start Mobile Menu Wrapper -->
            <div class="res-mobile-menu">
              <!-- Note Content Auto Generate By Jquery From Main Menu -->
            </div>
            <!-- End Mobile Menu Wrapper -->
          </div>
          <!-- Off Canvas Footer -->
          <div class="off-canvas-footer">

            <div class="side-footer">
              <div class="widget-social-icons">
                <a class="icon-color" href="#/"><i class="social social_facebook"></i></a>
                <a class="icon-color color-twitter" href="#/"><i class="social social_twitter"></i></a>
                <a class="icon-color color-instagram" href="#/"><i class="social social_instagram"></i></a>
                <a class="icon-color color-googleplus" href="#/"><i class="social social_googleplus"></i></a>
              </div>
              <div class="widget-copyright">
                <p> © backlink 2020. All Rights Reseverd</p>
              </div>
            </div>

          </div>
        </div>
        <!-- End Off Canvas Content Wrapper -->
      </div>
    </aside>
    <!--== End Side Menu ==-->

  </div>


  <!--=======================Javascript============================-->

  <!--=== Modernizr Min Js ===-->
  <script src="assets/js/modernizr-3.6.0.min.js"></script>
  <!--=== jQuery Min Js ===-->
  <script src="assets/js/jquery-3.3.1.min.js"></script>
  <!--=== jQuery Migration Min Js ===-->
  <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
  <!--=== Popper Min Js ===-->
  <script src="assets/js/popper.min.js"></script>
  <!--=== Bootstrap Min Js ===-->
  <script src="assets/js/bootstrap.min.js"></script>
  <!--=== CounterUp Min Js ===-->
  <script src="assets/js/counterup.min.js"></script>
  <!--=== Waypoint Min Js ===-->
  <script src="assets/js/waypoint.min.js"></script>
  <!--=== jquery UI Min Js ===-->
  <script src="assets/js/jquery-ui.min.js"></script>
  <!--=== Plugin Collection Js ===-->
  <script src="assets/js/plugincollection.js"></script>
  <!--=== Isotope Min Js ===-->
  <script src="assets/js/isotope.pkgd.min.js"></script>


  <!--=== Custom Js ===-->
  <script src="assets/js/custom.js"></script>

</body>


<!-- Mirrored from htmldemo.hasthemes.com/backlink/backlink/index-seo.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Jun 2021 10:01:18 GMT -->

</html>