<?php
include('inc/header.php')
?>
  <main class="main-content site-wrapper-reveal">
    <!--== Start Coming Soon Area Wrapper ==-->
    <section class="coming-soon-area" data-bg-img="assets/img/photos/coming-soon.jpg">
      <div class="container">
        <!-- <div class="logo">
          <a href="index-2.html"><img class="logo-main" src="assets/img/logo.png" alt="Logo" /></a>
        </div> -->
        <div class="row">
          <div class="col-lg-7 col-xl-6">
            <div class="coming-soon-content mb-0">
              <h5>Something new is</h5>
              <h2>Coming Soon</h2>
              <div class="countdown-content">
                <ul class="countdown-timer" id="countdown-timer">
                  <li><span class="days">00</span><p class="days_text">Days</p></li>
                  <li class="seperator">:</li>
                  <li><span class="hours">00</span><p class="hours_text">Hours</p></li>
                  <li class="seperator">:</li>
                  <li><span class="minutes">00</span><p class="minutes_text">Minutes</p></li>
                  <li class="seperator">:</li>
                  <li><span class="seconds">00</span><p class="seconds_text">Seconds</p></li>
                </ul>
              </div>
              <form class="input-btn-group">
                <input class="form-control no-border" type="text" placeholder="Email">
                <button class="btn btn-theme" type="button">Subscribe</button>
              </form>
            </div>
          </div>
        </div>
        <div class="widget-social-icons">
          <a class="facebook" href="#/"><i class="social_facebook"></i></a>
          <a class="twitter" href="#/"><i class="social_twitter"></i></a>
          <a class="twitter" href="#/"><i class="social_pinterest"></i></a>
          <a class="pinterest" href="#/"><i class="social_vimeo"></i></a>
          <a class="googleplus" href="#/"><i class="social_share"></i></a>
        </div>
      </div>
    </section>
    <!--== End Coming Soon Area Wrapper ==-->
  </main>
  <?php
include('inc/footer.php')
?>

	<script>
  jQuery(document).ready(function($) {
  // Countdown JS
    var now = new Date();
    var day = now.getDate();
    var month = now.getMonth() + 1;
    var year = now.getFullYear() + 1;

    var nextyear = month + '/' + day + '/' + year + ' 07:07:07';

    $('#countdown-timer').countdown({
      date: '10/13/2020 23:59:59', // TODO Date format: 07/27/2017 17:00:00
      offset: +2, // TODO Your Timezone Offset
      day: 'Day',
      days: 'Days',
      hideOnComplete: true
    }, function (container) {
      alert('Done!');
    });
  });
</script>

